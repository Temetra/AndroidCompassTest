package com.temetra.compasstest;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

public class MarkerView extends View {
    private ShapeDrawable marker;

    public MarkerView(Context context) {
        super(context);
    }

    public MarkerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MarkerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Set height to equal width
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);

        // Convert line width from dp to px
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1.5f, r.getDisplayMetrics());

        // Set line to center of view
        float halfWidth = width * 0.5f;

        // Create line
        marker = new ShapeDrawable(new RectShape());
        marker.getPaint().setColor(Color.RED);
        marker.setBounds((int)(halfWidth - px), 0, (int)(halfWidth + px), width);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        marker.draw(canvas);
    }
}
