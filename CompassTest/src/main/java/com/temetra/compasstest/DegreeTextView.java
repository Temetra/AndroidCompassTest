package com.temetra.compasstest;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

public class DegreeTextView extends TextView {
    float targetAzimuth;

    public DegreeTextView(Context context) {
        super(context);
    }

    public DegreeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DegreeTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        setText(String.valueOf(Math.round(targetAzimuth < 0f ? 360f + targetAzimuth : targetAzimuth)) + getResources().getString(R.string.degrees));
        super.onDraw(canvas);
    }

    public void updateDisplay(final float newAzimuth) {
        targetAzimuth = newAzimuth;
        invalidate();
    }
}
