package com.temetra.compasstest;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.view.WindowManager;

public class BaseActivity extends Activity implements SensorEventListener {
    float sensorLowPassAlpha = 0.5f;
    SensorManager sensorManager;
    Sensor accelerometerSensor;
    Sensor magneticFieldSensor;
    float[] rotation = new float[9];
    float[] inclination = new float[9];
    float[] gravity = new float[3];
    float[] geomagnetic = new float[3];
    float[] orientation = new float[3]; // azimuth (z), pitch (x), roll (y)
    CompassView compassView;
    DegreeTextView azimuthView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Stop display from sleeping
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Init sensor vars
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        magneticFieldSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // Views
        compassView = (CompassView)findViewById(R.id.compass);
        azimuthView = (DegreeTextView)findViewById(R.id.azimuth);
    }

    @Override
    protected void onResume() {
        // Register sensor listeners
        sensorManager.registerListener(this, magneticFieldSensor, SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_UI);
        super.onResume();
    }

    @Override
    protected void onPause() {
        // Unregister sensor listeners
        sensorManager.unregisterListener(this, magneticFieldSensor);
        sensorManager.unregisterListener(this, accelerometerSensor);
        super.onPause();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // Mag field
        if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            lowPassFilter(sensorEvent.values, geomagnetic);

        // Accel
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            lowPassFilter(sensorEvent.values, gravity);

        // Get rotation matrix
        boolean success = SensorManager.getRotationMatrix(rotation, inclination, gravity, geomagnetic);

        // Get orientation, update view
        if (success) {
            // Get azimuth
            SensorManager.getOrientation(rotation, orientation);
            float newAzimuth = (float)Math.toDegrees(orientation[0]);

            // Set compass rotation
            compassView.updateOrientation(newAzimuth);

            // Set heading text
            azimuthView.updateDisplay(newAzimuth);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private void lowPassFilter(final float[] sensorValues, final float[] prevValues) {
        prevValues[0] = prevValues[0] + sensorLowPassAlpha * (sensorValues[0] - prevValues[0]);
        prevValues[1] = prevValues[1] + sensorLowPassAlpha * (sensorValues[1] - prevValues[1]);
        prevValues[2] = prevValues[2] + sensorLowPassAlpha * (sensorValues[2] - prevValues[2]);
    }
}
