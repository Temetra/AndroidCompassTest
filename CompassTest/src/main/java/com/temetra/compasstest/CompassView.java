package com.temetra.compasstest;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CompassView extends ImageView {
    float targetAzimuth;
    float currentAzimuth;
    float compassLowPassAlpha = 0.15f;

    public CompassView(Context context) {
        super(context);
    }

    public CompassView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CompassView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        currentAzimuth = constrainAngle(currentAzimuth + compassLowPassAlpha * constrainAngle(targetAzimuth - currentAzimuth));
        setRotation(-currentAzimuth);
        super.onDraw(canvas);
        invalidate();
    }

    private float constrainAngle(float x){
        x = (x + 180f) % 360f;
        if (x < 0f) x += 360f;
        return x - 180f;
    }

    public void updateOrientation(final float newAzimuth) {
        targetAzimuth = newAzimuth;
    }
}
